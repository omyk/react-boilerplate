const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = (env={}, argv={}) => {
	return {
		entry: "./src/index.js",
		output: {
			path: path.join(__dirname, "/dist"),
			filename: "index_bundle.js"
		},
		devtool: 'source-map',
	  module: {
	    rules: [
	      {
	        test: /\.js$/,
	        exclude: /node_modules/,
	        use: {
	          loader: "babel-loader"
	        },
	      },
	      {
	        test: /\.css$/,
	        use: ["style-loader", "css-loader"]
	      },
				{
					test: /\.(gif|png|jpg|jpe?g|svg)$/i,
					use: [
						"file-loader",
						{
							loader: "image-webpack-loader",
							options: {
								disable: true
							}
						}
					]
				}
	    ]
	  },
	  plugins: [
	    argv.mode === 'development'
			? new HtmlWebpackPlugin({
	      template: "./src/index.html"
	    })
			: null
	  ].filter(plugin => !!plugin)
	}
};
