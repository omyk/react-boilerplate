import React, { Component } from "react";

import Link from './Link.js';
import "../styles/App.css";

class App extends Component {
  render() {
    return (
      <div>
        <Link page="http://www.facebook.com">Facebook</Link>
      </div>
    );
  }
}

export default App;
